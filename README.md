# Wikipedia Survey 2023

Ready-to-use data survey from the Wikipedia Survey 2023, with convenient formats, groups of variables and some recoding

## Object produced

- `bdd` : survey's data
- `groups` : R list storing all groups of variable (useful for regression)
- `groups_df` : same as `groups` but in a data frame
- `size_groups` : R vector storing size of each groups in the order in which the groups are saved in `groups` (usefull for concrete manipulation)

## Files

- `main.R` : Main script to load and process the data
- `survey.RData` : Storage file to get all at once in R `bdd`, `groups`, `size_groups` and `groups_df`
- `data_filtered.csv` : Filtered dataset from the datapaper
- `readyToUse_*.csv` : CSV files containing same data as `survey.RData` but in separate files
- `interets.xlsx` : Excel file containing the list of naming taxonomies

## To be done

- reinforce the groups definition by adding more variables or more groups
- check recodings